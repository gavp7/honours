//app.ts
import { Component } from '@angular/core';
import { Platform, ionicBootstrap } from 'ionic-angular';
import { StatusBar } from 'ionic-native';
import { TabsPage } from './pages/tabs/tabs';
import { LoginPage } from './pages/login/login';
import {HomePage} from './pages/home/home';
@Component({
  template: '<ion-nav [root]="rootPage"></ion-nav>'
})
export class MyApp {

  public rootPage: any;

  constructor(private platform: Platform) {
      if((window.localStorage.getItem('username') === "undefined" || window.localStorage.getItem('username') === null) && 
     (window.localStorage.getItem('password') === "undefined" || window.localStorage.getItem('password') === null)) {
    this.rootPage = LoginPage;
  } else {
    this.rootPage = TabsPage;
  }

    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      StatusBar.styleDefault();
    });
  }
}

ionicBootstrap(MyApp);
