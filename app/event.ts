//evemt.ts
export class Event { 
	_id: number;
	name: string;
	sport: string;
	players: number;
	date: string;
	starttime: string;
	endtime: string;
	location: string;
	invited: string[];
	attending: string[];
	posts : [{ 
		author:string;
		post:string;
		date: string;
		reply: [{
			author: string;
			post: string;
			date: string;
		}]
	}]
}

