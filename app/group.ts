//group.ts
export class Group { 
	_id: number;
	name: string;
	members: string[];
	events: string[];
	posts : [{ 
		author:string;
		post:string;
		date: string;
		comments: [{ author:string;
			message: string;
			date: string;
		}]	
	}]
}

