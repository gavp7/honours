"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var ionic_angular_1 = require('ionic-angular');
var find_friends_1 = require('../find-friends/find-friends');
var friends_1 = require('../friends/friends');
var friend_requests_1 = require('../friend-requests/friend-requests');
var settings_1 = require('../settings/settings');
var user_1 = require('../../user');
var user_service_1 = require('../../providers/user-service/user-service');
var AccountPage = (function () {
    function AccountPage(userService, navCtrl) {
        this.userService = userService;
        this.navCtrl = navCtrl;
        this.email = window.localStorage.getItem('email');
        this.fullname = window.localStorage.getItem('name');
        this.city = window.localStorage.getItem('city');
        this.loadUsers();
    }
    AccountPage.prototype.doRefresh = function (refresher) {
        this.loadUsers();
        refresher.complete();
    };
    AccountPage.prototype.loadUsers = function () {
        var _this = this;
        this.userService.load()
            .subscribe(function (userList) {
            _this.users = userList;
            var elementPos = _this.users.map(function (x) { return x.email; }).indexOf(window.localStorage.getItem(window.localStorage.getItem('email')));
            _this.user = _this.users[elementPos];
            _this.RequestCount();
        });
    };
    AccountPage.prototype.logout = function () {
        window.localStorage.removeItem('name');
        window.localStorage.removeItem('password');
        window.localStorage.removeItem('city');
        window.localStorage.removeItem('email');
        window.location.reload(true);
    };
    AccountPage.prototype.GoToSettings = function () {
        this.navCtrl.push(settings_1.SettingsPage, { user: user_1.User });
    };
    AccountPage.prototype.FindFriends = function () {
        this.navCtrl.push(find_friends_1.FindFriendsPage, {});
    };
    AccountPage.prototype.GoToFriends = function () {
        this.navCtrl.push(friends_1.FriendsPage, {});
    };
    AccountPage.prototype.FriendRequests = function () {
        this.navCtrl.push(friend_requests_1.FriendRequestsPage, {});
    };
    AccountPage.prototype.RequestCount = function () {
        //get current user
        var user;
        var elementPos = this.users.map(function (x) { return x.email; }).indexOf(window.localStorage.getItem('email'));
        user = this.users[elementPos];
        this.count = user.friendrequests.length;
    };
    AccountPage = __decorate([
        core_1.Component({
            templateUrl: 'build/pages/account/account.html',
            providers: [user_service_1.UserService]
        }), 
        __metadata('design:paramtypes', [user_service_1.UserService, ionic_angular_1.NavController])
    ], AccountPage);
    return AccountPage;
}());
exports.AccountPage = AccountPage;
