//account.ts
import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { LoginPage } from '../login/login';
import { FindFriendsPage } from '../find-friends/find-friends';
import {FriendsPage} from '../friends/friends';
import {FriendRequestsPage } from '../friend-requests/friend-requests';
import {SettingsPage} from '../settings/settings';
import {User} from '../../user';
import {UserService} from '../../providers/user-service/user-service';
import {Observable} from 'rxjs/Observable';


@Component({
	templateUrl: 'build/pages/account/account.html',
	providers: [UserService]
})

export class AccountPage {
	public user: User;
	public users: User[];
	public fullname: string;
	public email: string;
	public city: string;
	public count: number;
	constructor(public userService: UserService, 
		private navCtrl: NavController) {
		this.email = window.localStorage.getItem('email');
		this.fullname = window.localStorage.getItem('name');
		this.city = window.localStorage.getItem('city');
		this.loadUsers();  

		//auto refreshes account page every 5 second, retrieving any new friend requests
		Observable.interval(5000).subscribe(x => {
    this.loadUsers();
  });
	}
	//functions to be called when page is dragged down to be refreshed
	doRefresh(refresher) {
		this.loadUsers();
		refresher.complete();
	}

	
	//loads a list of users from the database
	loadUsers() {
		this.userService.load()
		.subscribe(userList => {
			this.users = userList;
			var elementPos = this.users.map(function(x) {return x.email; }).indexOf(window.localStorage.getItem(window.localStorage.getItem('email')));
			this.user = this.users[elementPos];		//finds current user
			this.RequestCount();
		})
	}

	logout(): void { //logout removes user details and reloads screen, forwarding to login page
		window.localStorage.removeItem('name');
		window.localStorage.removeItem('password');
		window.localStorage.removeItem('city');
		window.localStorage.removeItem('email');
		window.location.reload(true);
	}  

	//functions for navigating to other pages
	GoToSettings(){
		this.navCtrl.push(SettingsPage, {user: User});
	}
	FindFriends() {	
		this.navCtrl.push(FindFriendsPage, {});
	}

	GoToFriends() {
		this.navCtrl.push(FriendsPage, {});
	}
	FriendRequests(){
		this.navCtrl.push(FriendRequestsPage, {});
	}

	RequestCount(){
		//get current user
		var user:User; 
		var elementPos = this.users.map(function(x) {return x.email; }).indexOf(window.localStorage.getItem('email'));
		user = this.users[elementPos];
		//returns the number of friend requests to be displayed
		this.count = user.friendrequests.length;
	}
}
