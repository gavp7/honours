"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var ionic_angular_1 = require('ionic-angular');
var user_service_1 = require('../../providers/user-service/user-service');
/*
  Generated class for the FindFriendsPage page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
  */
var FindFriendsPage = (function () {
    function FindFriendsPage(userService, navCtrl) {
        this.userService = userService;
        this.navCtrl = navCtrl;
        this.loadUsers();
    }
    FindFriendsPage.prototype.loadUsers = function () {
        var _this = this;
        this.userService.load()
            .subscribe(function (userList) {
            _this.users = userList;
        });
    };
    FindFriendsPage.prototype.addFriend = function (_email) {
        var _this = this;
        this.loadUsers();
        //get current user
        var user;
        var elementPos = this.users.map(function (x) { return x.email; }).indexOf(_email);
        user = this.users[elementPos];
        user.friendrequests = user.friendrequests || [];
        user.friendrequests.push(window.localStorage.getItem('email'));
        this.userService.update(user)
            .subscribe(function (res) {
            _this.navCtrl.pop();
        });
    };
    FindFriendsPage.prototype.getItems = function (ev) {
        // Reset items back to all of the items
        this.tempusers = this.users;
        // set val to the value of the searchbar
        var val = ev.target.value;
        // if the value is an empty string don't filter the items
        if (val && val.trim() != '') {
            this.tempusers = this.tempusers.filter(function (user) {
                return (user.name.toLowerCase().indexOf(val.toLowerCase()) > -1);
            });
        }
    };
    FindFriendsPage = __decorate([
        core_1.Component({
            templateUrl: 'build/pages/find-friends/find-friends.html',
            providers: [user_service_1.UserService]
        }), 
        __metadata('design:paramtypes', [user_service_1.UserService, ionic_angular_1.NavController])
    ], FindFriendsPage);
    return FindFriendsPage;
}());
exports.FindFriendsPage = FindFriendsPage;
