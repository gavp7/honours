//find-friends.ts
import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import {UserService} from '../../providers/user-service/user-service';
import {User} from '../../user';

  @Component({
  	templateUrl: 'build/pages/find-friends/find-friends.html',
  	providers: [UserService]

  })
  export class FindFriendsPage {
  	public users: User[];
  	public tempusers: User[];

  	constructor(public userService: UserService, 		private navCtrl: NavController) {
  		this.loadUsers();
  	}

    //loads a list of users
    loadUsers() {
      this.userService.load()
      .subscribe(userList => {
        this.users = userList;
      })
    }

    //this function checks if the user is in the current users friend list. If so, the 'add friend' button will not be displayed in the app
    checkFriend(friend: string){
      var elementPos = this.users.map(function(x) {return x.email; }).indexOf(window.localStorage.getItem('email'));
      var user = this.users[elementPos];
      var check: boolean;
      check = true;
      for(var i=0; i < user.friends.length; i++)
      {
        if(friend == user.friends[i] || friend.toLowerCase() == user.email.toLowerCase())
        {
          check = false;
          break;
        }
      }
      return check;
    }

    //add a friend
    addFriend(_email:string){
      this.loadUsers();
      //get current user
      var user: User;
      var elementPos = this.users.map(function(x) {return x.email; }).indexOf(_email);
      user = this.users[elementPos];
      user.friendrequests = user.friendrequests || [];
      user.friendrequests.push(window.localStorage.getItem('email'));
      this.userService.update(user) //adds the users name to the selected friends 'friend requests' and updates database
      .subscribe(res => {
        this.navCtrl.pop();
      });
    }

    //updates items returned based on the input box
    getItems(ev: any) {
      // Reset items back to all of the items
      this.tempusers = this.users;
      // set val to the value of the searchbar
      let val = ev.target.value;

      // if the value is an empty string don't filter the items
      if (val && val.trim() != '') {
        this.tempusers = this.tempusers.filter((user) => {
          return (user.name.toLowerCase().indexOf(val.toLowerCase()) > -1);
        })
      }
    }

  }
