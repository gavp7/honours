"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var ionic_angular_1 = require('ionic-angular');
var user_service_1 = require('../../providers/user-service/user-service');
/*
  Generated class for the FriendRequestsPage page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
  */
var FriendRequestsPage = (function () {
    function FriendRequestsPage(userService, navCtrl) {
        this.userService = userService;
        this.navCtrl = navCtrl;
        this.loadUsers();
    }
    FriendRequestsPage.prototype.doRefresh = function (refresher) {
        this.loadUsers();
        refresher.complete();
    };
    FriendRequestsPage.prototype.loadUsers = function () {
        var _this = this;
        this.userService.load()
            .subscribe(function (userList) {
            _this.users = userList;
            _this.getRequests();
        });
    };
    FriendRequestsPage.prototype.getRequests = function () {
        //get current user
        var user;
        var elementPos = this.users.map(function (x) { return x.email; }).indexOf(window.localStorage.getItem('email'));
        user = this.users[elementPos];
        this.tempusers = [];
        user.friendrequests = user.friendrequests || [];
        //get all the users named in the friendrequests field
        for (var i = 0; i < user.friendrequests.length; i++) {
            for (var j = 0; j < this.users.length; j++) {
                if (user.friendrequests[i] == this.users[j].email) {
                    this.tempusers.push(this.users[j]);
                }
            }
        }
    };
    FriendRequestsPage.prototype.Accept = function (friend) {
        //get current user
        var user;
        var elementPos = this.users.map(function (x) { return x.email; }).indexOf(window.localStorage.getItem('email'));
        user = this.users[elementPos];
        //add user and friends emails to their 'friends' field
        user.friends.push(friend.email);
        friend.friends.push(user.email);
        //remove the friend request from the friendrequests field
        for (var i = 0; i < user.friendrequests.length; i++) {
            if (user.friendrequests[i] = friend.email) {
                user.friendrequests.splice(i, 1);
            }
        }
        //save changes
        this.userService.update(user)
            .subscribe(function (res) {
            console.log('user updated');
        });
        this.userService.update(friend)
            .subscribe(function (res) {
            console.log('friend updated');
        });
        this.navCtrl.pop();
    };
    FriendRequestsPage.prototype.Decline = function (friend) {
        var _this = this;
        var user;
        var elementPos = this.users.map(function (x) { return x.email; }).indexOf(window.localStorage.getItem('email'));
        user = this.users[elementPos];
        //remove the friend request from the friendrequests field
        for (var i = 0; i < user.friendrequests.length; i++) {
            if (user.friendrequests[i] = friend.email) {
                user.friendrequests.splice(i, 1);
            }
        }
        //save changes
        this.userService.update(user)
            .subscribe(function (res) {
            console.log('user updated');
            _this.navCtrl.push(FriendRequestsPage, {});
        });
        this.navCtrl.pop();
    };
    FriendRequestsPage = __decorate([
        core_1.Component({
            templateUrl: 'build/pages/friend-requests/friend-requests.html',
            providers: [user_service_1.UserService]
        }), 
        __metadata('design:paramtypes', [user_service_1.UserService, ionic_angular_1.NavController])
    ], FriendRequestsPage);
    return FriendRequestsPage;
}());
exports.FriendRequestsPage = FriendRequestsPage;
