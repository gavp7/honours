//friend-requests.ts
import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import {UserService} from '../../providers/user-service/user-service';
import {User} from '../../user';
import {Observable} from 'rxjs/Observable';

@Component({
  templateUrl: 'build/pages/friend-requests/friend-requests.html',
  providers: [UserService]

})
export class FriendRequestsPage {
  public users: User[];
  public tempusers: User[];

  constructor(public userService: UserService, 
    private navCtrl: NavController) {
    this.loadUsers();
    //auto updates page every minute
    Observable.interval(1000 * 60).subscribe(x => {
      this.loadUsers();
    });
  }

  //refresh when page is dragged down
  doRefresh(refresher) {
    this.loadUsers();
    refresher.complete();
  }

  loadUsers() { //load a list of all users
    this.userService.load()
    .subscribe(userList => {
      this.users = userList;
      this.getRequests();
    })
  }

  //retrieves all friend requests
  getRequests() { 
    //get current user
    var user:User; 
    var elementPos = this.users.map(function(x) {return x.email; }).indexOf(window.localStorage.getItem('email'));
    user = this.users[elementPos];
    this.tempusers = [];
    user.friendrequests = user.friendrequests || [];
    //get all the users named in the friendrequests field
    for(var i =0; i < user.friendrequests.length; i++){
      for(var j = 0; j < this.users.length; j++){
        if(user.friendrequests[i]==this.users[j].email){
          this.tempusers.push(this.users[j]);
        }
      }
    }
  }


  Accept(friend: User){
    //get current user
    var user:User; 
    var elementPos = this.users.map(function(x) {return x.email; }).indexOf(window.localStorage.getItem('email'));
    user = this.users[elementPos];
    //add user and friends emails to their 'friends' field
    user.friends.push(friend.email);
    friend.friends.push(user.email);

    //remove the friend request from the friendrequests field
    for(var i =0; i < user.friendrequests.length; i++){
      if(user.friendrequests[i] = friend.email)
      {
        user.friendrequests.splice(i, 1);
      }
    }
    //save changes
    this.userService.update(user)
    .subscribe(res => {
    });
    this.userService.update(friend)
    .subscribe(res => {
    });            
    this.navCtrl.pop();

  }

  Decline(friend: User){
    var user:User; 
    var elementPos = this.users.map(function(x) {return x.email; }).indexOf(window.localStorage.getItem('email'));
    user = this.users[elementPos];

    //remove the friend request from the friendrequests field
    for(var i =0; i < user.friendrequests.length; i++){
      if(user.friendrequests[i] = friend.email)
      {
        user.friendrequests.splice(i, 1);
      }
    }
    //save changes
    this.userService.update(user)
    .subscribe(res => {
      console.log('user updated');
      this.navCtrl.push(FriendRequestsPage, {});
    });
    this.navCtrl.pop();

  }
}
