//friends.ts
import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import {UserService} from '../../providers/user-service/user-service';
import {User} from '../../user';
import {Observable} from 'rxjs/Observable';

@Component({
  templateUrl: 'build/pages/friends/friends.html',
  providers: [UserService]
})
export class FriendsPage {
  public users: User[];
  public friends: User[];
  public user: User;

  constructor(public userService: UserService,
    private navCtrl: NavController) {
    this.loadUsers();

    //auto update page every minute
    Observable.interval(1000 * 60).subscribe(x => {
      this.loadUsers();
    });
  }

  doRefresh(refresher) { //refresh when page is dragged down
    this.loadUsers();
    refresher.complete();
  }

  loadUsers() { //load all users
    this.userService.load()
    .subscribe(userList => {
      this.users = userList;
      var elementPos = this.users.map(function(x) {return x.email; }).indexOf(window.localStorage.getItem('email'));
      this.user = this.users[elementPos];
      this.getFriends();
    })
  }

  getFriends() { //retrieve all friends from user friend list
    this.friends = [];
    if(this.user.friends != null){
      for(var i =0; i < this.user.friends.length; i++){
           var elementPos = this.users.map(function(x) {return x.email; }).indexOf(this.user.friends[i]);
            this.friends.push(this.users[elementPos]);
          }
        }
      }
   }
