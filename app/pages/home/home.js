"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var ionic_angular_1 = require('ionic-angular');
var new_game_1 = require('../new-game/new-game');
var user_service_1 = require('../../providers/user-service/user-service');
var event_service_1 = require('../../providers/event-service/event-service');
var group_service_1 = require('../../providers/group-service/group-service');
var view_game_1 = require('../view-game/view-game');
var viewgroup_1 = require('../viewgroup/viewgroup');
var newgroup_1 = require('../newgroup/newgroup');
var HomePage = (function () {
    function HomePage(userService, eventService, navCtrl, groupService) {
        this.userService = userService;
        this.eventService = eventService;
        this.navCtrl = navCtrl;
        this.groupService = groupService;
        this.email = window.localStorage.getItem('email');
        this.fullname = window.localStorage.getItem('name');
        this.loadUsers();
    }
    HomePage.prototype.doRefresh = function (refresher) {
        this.loadUsers();
        refresher.complete();
    };
    HomePage.prototype.loadUsers = function () {
        var _this = this;
        this.userService.load()
            .subscribe(function (userList) {
            _this.users = userList;
            //get current user
            var elementPos = _this.users.map(function (x) { return x.email; }).indexOf(_this.email);
            _this.loadEvents();
        });
    };
    HomePage.prototype.loadEvents = function () {
        var _this = this;
        this.eventService.load()
            .subscribe(function (eventList) {
            _this.events = eventList;
            _this.GetUserEvents();
        });
    };
    HomePage.prototype.LoadGroups = function () {
        var _this = this;
        this.groupService.load()
            .subscribe(function (groupList) {
            _this.groups = groupList;
            _this.GetUserGroups();
        });
    };
    HomePage.prototype.NewGame = function () {
        this.navCtrl.push(new_game_1.NewGamePage, {});
    };
    HomePage.prototype.NewGroup = function () {
        this.navCtrl.push(newgroup_1.NewgroupPage, {});
    };
    HomePage.prototype.ViewGame = function (_event) {
        this.navCtrl.push(view_game_1.ViewGamePage, { event: _event });
    };
    HomePage.prototype.ViewGroup = function (_group) {
        this.navCtrl.push(viewgroup_1.ViewgroupPage, { group: _group });
    };
    HomePage.prototype.GetUserGroups = function () {
        this.tempgroups = [];
        this.user.groups = this.user.groups || [];
        for (var i = 0; i < this.user.groups.length; i++) {
            for (var j = 0; j < this.groups.length; j++) {
                if (this.user.groups[i] == this.groups[j].name) {
                    this.tempgroups.push(this.groups[j]);
                }
            }
        }
    };
    HomePage.prototype.GetUserEvents = function () {
        //get current user
        var now = new Date();
        var elementPos = this.users.map(function (x) { return x.email; }).indexOf(this.email);
        this.user = this.users[elementPos];
        this.tempevents = [];
        this.user.events = this.user.events || [];
        for (var i = 0; i < this.user.events.length; i++) {
            for (var j = 0; j < this.events.length; j++) {
                if (this.user.events[i] == this.events[j].name) {
                    if (this.events[j].date > now) {
                        this.tempevents.push(this.events[j]);
                    }
                }
            }
        }
        this.LoadGroups();
        console.log(this.tempevents.length);
    };
    HomePage = __decorate([
        core_1.Component({
            templateUrl: 'build/pages/home/home.html',
            providers: [user_service_1.UserService, event_service_1.EventService, group_service_1.GroupService]
        }), 
        __metadata('design:paramtypes', [user_service_1.UserService, event_service_1.EventService, ionic_angular_1.NavController, group_service_1.GroupService])
    ], HomePage);
    return HomePage;
}());
exports.HomePage = HomePage;
