//home.ts
import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { NewGamePage } from '../new-game/new-game';
import {UserService} from '../../providers/user-service/user-service';
import {User} from '../../user';
import {EventService } from '../../providers/event-service/event-service';
import {Event} from '../../event';
import {GroupService} from '../../providers/group-service/group-service';
import {Group} from '../../group';
import { ViewGamePage } from '../view-game/view-game';
import {ViewgroupPage} from '../viewgroup/viewgroup';
import {NewgroupPage} from '../newgroup/newgroup';
import {Observable} from 'rxjs/Observable';
@Component({
	templateUrl: 'build/pages/home/home.html',
	providers: [UserService, EventService, GroupService]

})
export class HomePage {

	public fullname: string;
	public email: string;
	public users: User[];
	public events: Event[];
	public groups: Group[];
	public tempgroups: Group[];
	public tempevents: Event[];
	public user: User;

	constructor(public userService: UserService,
		public eventService: EventService,
		public navCtrl: NavController,
		public groupService: GroupService) {
		this.email = window.localStorage.getItem('email');
		this.fullname = window.localStorage.getItem('name');

		this.loadUsers();

		Observable.interval(1000 * 60).subscribe(x => {
			this.loadUsers(); //auto updates every minute
			this.loadEvents();
		});
	}

	//refresh when page dragged down
	doRefresh(refresher) {
		this.loadUsers();
		this.loadEvents();
		refresher.complete();
	}

	loadUsers() { //load all users
		this.userService.load()
		.subscribe(userList => {
			this.users = userList;
			//get current user
			var elementPos = this.users.map(function(x) {return x.email; }).indexOf(this.email);
			this.loadEvents();
		})
	}

	loadEvents() {
		this.eventService.load()
		.subscribe(eventList => {
			this.events = eventList;
			this.GetUserEvents();
		})
	}

	LoadGroups(){
		this.groupService.load()
		.subscribe(groupList => {
			this.groups = groupList;
			this.GetUserGroups();
		})
	}

	//navigation functions
	NewGame() {
		this.navCtrl.push(NewGamePage, {});
	}

	NewGroup() {
		this.navCtrl.push(NewgroupPage, {});
	}

	ViewGame(_event: Event) {
		this.navCtrl.push(ViewGamePage, { event: _event});
	}
	ViewGroup(_group: Group) {
		this.navCtrl.push(ViewgroupPage, { group: _group});
	}

	GetUserGroups(){
		this.tempgroups = [];
		this.user.groups = this.user.groups || [];

		for(var i =0; i < this.user.groups.length; i++) {
			for(var j =0; j < this.groups.length; j++){
				if(this.user.groups[i] == this.groups[j].name) {

					this.tempgroups.push(this.groups[j]);
				}
			}
		}
	}

	GetUserEvents() {
		//get current user
		var elementPos = this.users.map(function(x) {return x.email; }).indexOf(this.email);
		this.user = this.users[elementPos];
		this.tempevents = [];
		this.user.events = this.user.events ||[];
		for(var i =0; i < this.user.events.length; i++)
		 {
			var elementPos = this.user.events.map(function(x) {return x; }).indexOf(this.user.events[i]);
			this.tempevents.push(this.events[elementPos]);	
			}
			this.tempevents.reverse();
			this.LoadGroups();
		}
	}
