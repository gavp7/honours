//login.ts
import { Component } from '@angular/core';
import { NavController, ToastController } from 'ionic-angular';
import { RegisterPage } from '../register/register'
import {UserService} from '../../providers/user-service/user-service';
import { TabsPage } from '../tabs/tabs';
import {User} from '../../user';

@Component({
	templateUrl: 'build/pages/login/login.html',
	providers: [UserService]

})

export class LoginPage {

	public users: User[];

	constructor(public navCtrl: NavController,
		public userService: UserService,
		public toastCtrl: ToastController) {
		this.loadUsers();
	}

	loadUsers() { //load users
		this.userService.load()
		.subscribe(userList => {
			this.users = userList;
		})
	}

	Authenticate(_email: string, _password: string) { //authenticate users
		var elementPos = this.users.map(function(x) {return x.email.toLowerCase(); }).indexOf(_email.toLowerCase());
		var objectFound = this.users[elementPos];
		
		if(objectFound == null)
		{
			this.LoginFailToast();
		}
		else 
		{
			if(objectFound.password == _password) //TODO - password encryption!!! 
			{
				window.localStorage.setItem('name', objectFound.name);
				window.localStorage.setItem('email', objectFound.email);
				window.localStorage.setItem('password', objectFound.password);
				this.navCtrl.push(TabsPage, {});
				this.LoginSuccessToast();		

			}
			else 
			{
				this.LoginFailToast();
			}
		}

	}
	
	Register() {
		this.navCtrl.push(RegisterPage, {});
	}

	LoginSuccessToast() {
		let toast = this.toastCtrl.create({
			message:'Login Successful!',
			duration: 3000
		});
		toast.present();
	}

	LoginFailToast() {
		let toast = this.toastCtrl.create({
			message:'Incorrect Email or Password!',
			duration: 3000
		});
		toast.present();
	}

}

