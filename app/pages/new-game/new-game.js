"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var ionic_angular_1 = require('ionic-angular');
var ionic_angular_2 = require('ionic-angular');
var user_service_1 = require('../../providers/user-service/user-service');
var event_service_1 = require('../../providers/event-service/event-service');
var group_service_1 = require('../../providers/group-service/group-service');
var home_1 = require('../home/home');
/*
  Generated class for the NewGamePage page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
  */
var NewGamePage = (function () {
    function NewGamePage(eventService, toastCtrl, userService, modalCtrl, navCtrl, groupService, navParams) {
        this.eventService = eventService;
        this.toastCtrl = toastCtrl;
        this.userService = userService;
        this.modalCtrl = modalCtrl;
        this.navCtrl = navCtrl;
        this.groupService = groupService;
        this.navParams = navParams;
        this.date = new Date();
        this.starttime = new Date();
        this.endtime = new Date();
        this.loadUsers();
        if (navParams.get('group') != null) {
            this.usergroup = navParams.get('group');
        }
    }
    NewGamePage.prototype.loadUsers = function () {
        var _this = this;
        this.userService.load()
            .subscribe(function (userList) {
            _this.users = userList;
            var elementPos = _this.users.map(function (x) { return x.email; }).indexOf(window.localStorage.getItem('email'));
            _this.user = _this.users[elementPos];
            _this.getFriends();
        });
    };
    NewGamePage.prototype.getFriends = function () {
        this.friends = [];
        for (var i = 0; i < this.user.friends.length; i++) {
            for (var j = 0; j < this.users.length; j++) {
                if (this.user.friends[i] == this.users[j].email) {
                    this.friends.push(this.users[j]);
                    console.log(this.users[j]);
                }
            }
        }
    };
    NewGamePage.prototype.createGame = function (_name, _sport, _players, _date, _starttime, _endtime, _location, _invited) {
        var _this = this;
        var attending;
        attending = [];
        attending.push(this.user.name);
        if (_name == null || _sport == null || _players == null || _date == null || _starttime == null || _endtime == null || location == null) {
            var toast = this.toastCtrl.create({
                message: 'Please fill in all required fields',
                duration: 3000
            });
            toast.present();
        }
        for (var i = 1; i < _players; i++) {
            attending.push('Slot Available');
        }
        this.tempusers = [];
        _invited = _invited || [];
        _invited.push(window.localStorage.getItem('email'));
        //get all invited users and store the event to their database collection 
        for (var i = 0; i < _invited.length; i++) {
            for (var j = 0; j < this.users.length; j++) {
                if (_invited[i] == this.users[j].email) {
                    this.tempusers.push(this.users[j]);
                }
            }
        }
        //If the game has been created within a group, add to the group page
        if (this.navParams.get('group') != null) {
            this.usergroup.events.push(_name);
            this.groupService.update(this.usergroup).subscribe(function (newEvent) { });
        }
        for (var i = 0; i < this.tempusers.length; i++) {
            this.tempusers[i].events = this.tempusers[i].events || [];
            this.tempusers[i].events.push(_name);
            this.userService.update(this.tempusers[i])
                .subscribe(function (res) {
                if (_name == null) {
                    var toast = _this.toastCtrl.create({
                        message: 'Game Created!',
                        duration: 3000
                    });
                    toast.present();
                }
            });
        }
        //Save event to database
        this.eventService.add(_name, _sport, _players, _date, _starttime, _endtime, _location, _invited, attending)
            .subscribe(function (newEvent) {
            _this.navCtrl.push(home_1.HomePage, {});
        });
    };
    NewGamePage = __decorate([
        core_1.Component({
            templateUrl: 'build/pages/new-game/new-game.html',
            providers: [user_service_1.UserService, event_service_1.EventService, group_service_1.GroupService]
        }), 
        __metadata('design:paramtypes', [event_service_1.EventService, ionic_angular_1.ToastController, user_service_1.UserService, ionic_angular_2.ModalController, ionic_angular_1.NavController, group_service_1.GroupService, ionic_angular_1.NavParams])
    ], NewGamePage);
    return NewGamePage;
}());
exports.NewGamePage = NewGamePage;
