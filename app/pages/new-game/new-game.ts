//new-game.ts

//Still to do* Implement Google Maps!
import { Component } from '@angular/core';
import { NavController, ToastController, NavParams } from 'ionic-angular';
import { ModalController } from 'ionic-angular';
import {UserService} from '../../providers/user-service/user-service';
import {EventService } from '../../providers/event-service/event-service';
import {GroupService } from '../../providers/group-service/group-service';
import {Group } from '../../group';
import {User} from '../../user';
import { HomePage } from '../home/home';

@Component({
  templateUrl: 'build/pages/new-game/new-game.html',
  providers: [UserService, EventService, GroupService]
})
export class NewGamePage {

  public date: string = new Date().toISOString();
  public starttime: string = new Date().toISOString();
  public endtime: string = new Date().toISOString();
  public users: User[];
  public friends: User[];
  public tempusers: User[];
  public user: User;
  public usergroup: Group;

  constructor(public eventService: EventService,
    public toastCtrl: ToastController,
    public userService: UserService, 
    public modalCtrl: ModalController, 
    private navCtrl: NavController,
    public groupService: GroupService,
    public navParams: NavParams) {
    this.loadUsers();
    //if there is a navparam, the event has been called from a group page and the group is set
    if(navParams.get('group') != null){ 
      this.usergroup = navParams.get('group');
    }
  }

  loadUsers() {
    this.userService.load()
    .subscribe(userList => {
      this.users = userList;
      var elementPos = this.users.map(function(x) {return x.email; }).indexOf(window.localStorage.getItem('email'));
      this.user = this.users[elementPos];
      this.getFriends();

    })
  }

  getFriends() { //retrieve all friends from user friend list
    this.friends = [];
    if(this.user.friends != null){
      for(var i =0; i < this.user.friends.length; i++){
           var elementPos = this.users.map(function(x) {return x.email; }).indexOf(this.user.friends[i]);
            this.friends.push(this.users[elementPos]);
          }
        }
      }

  createGame(_name: string,_sport: string, _players: number, _date: string, _starttime: string, _endtime: string, _location: string, _invited: string[]) {
    var attending: string[];
    attending = [];
    attending.push(this.user.name);
    if(_name == null || _sport == null || _players == null || _date == null || _starttime == null || _endtime ==null || location == null){
      let toast = this.toastCtrl.create({
        message:'Please fill in all required fields',
        duration: 3000
      });
      toast.present();
    }

    for(var i=1; i < _players; i++) {
      attending.push('Slot Available');
    }

    this.tempusers = [];
    _invited = _invited || [];
    _invited.push(window.localStorage.getItem('email'));

    //get all invited users and store the event to their database collection 
    for(var i =0; i < _invited.length; i++){
        var elementPos = this.users.map(function(x) {return x.email; }).indexOf(_invited[i]);
                    this.tempusers.push(this.users[elementPos]);
    }
    
    for(var i =0; i < this.tempusers.length; i++){
      this.tempusers[i].events = this.tempusers[i].events || [];
      this.tempusers[i].events.push(_name);
      this.userService.update(this.tempusers[i])
      .subscribe(res => {
        if(_name == null){
          let toast = this.toastCtrl.create({
            message:'Game Created!',
            duration: 3000
          });
          toast.present();
        }
      });


    }


    //Save event to database
    this.eventService.add(_name, _sport, _players, _date, _starttime, _endtime, _location, _invited, attending)
    .subscribe(newEvent  => {
      this.navCtrl.pop();
    });


    //If the game has been created within a group, add to the group page
    if(this.usergroup != undefined){
      console.log('yay');
      this.usergroup.events.push(_name);
      console.log('woo');
      //save changes
      this.groupService.update(this.usergroup)
      .subscribe(res => {
        console.log('user updated');
      });
    }
  }
}


