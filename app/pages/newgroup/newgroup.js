"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var ionic_angular_1 = require('ionic-angular');
var user_service_1 = require('../../providers/user-service/user-service');
var group_service_1 = require('../../providers/group-service/group-service');
var home_1 = require('../home/home');
/*
  Generated class for the NewgroupPage page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
  */
var NewgroupPage = (function () {
    function NewgroupPage(navCtrl, toastCtrl, userService, groupService) {
        this.navCtrl = navCtrl;
        this.toastCtrl = toastCtrl;
        this.userService = userService;
        this.groupService = groupService;
        this.loadUsers();
    }
    NewgroupPage.prototype.loadUsers = function () {
        var _this = this;
        this.userService.load()
            .subscribe(function (userList) {
            _this.users = userList;
            var elementPos = _this.users.map(function (x) { return x.email; }).indexOf(window.localStorage.getItem('email'));
            _this.user = _this.users[elementPos];
            _this.getFriends();
        });
    };
    NewgroupPage.prototype.getFriends = function () {
        this.friends = [];
        for (var i = 0; i < this.user.friends.length; i++) {
            for (var j = 0; j < this.users.length; j++) {
                if (this.user.friends[i] == this.users[j].email) {
                    this.friends.push(this.users[j]);
                    console.log(this.users[j]);
                }
            }
        }
    };
    NewgroupPage.prototype.createGame = function (_name, _invited) {
        var _this = this;
        if (_name == null) {
            var toast = this.toastCtrl.create({
                message: 'Please name your group!',
                duration: 3000
            });
            toast.present();
        }
        this.tempusers = [];
        _invited = _invited || [];
        _invited.push(window.localStorage.getItem('email'));
        //get all invited users and store the event to their database collection 
        for (var i = 0; i < _invited.length; i++) {
            for (var j = 0; j < this.users.length; j++) {
                if (_invited[i] == this.users[j].email) {
                    this.tempusers.push(this.users[j]);
                }
            }
        }
        for (var i = 0; i < this.tempusers.length; i++) {
            this.tempusers[i].groups = this.tempusers[i].groups || [];
            this.tempusers[i].groups.push(_name);
            this.userService.update(this.tempusers[i])
                .subscribe(function (res) {
                if (_name == null) {
                    var toast = _this.toastCtrl.create({
                        message: 'Group Created!',
                        duration: 3000
                    });
                    toast.present();
                }
            });
        }
        //Save event to database
        this.groupService.add(_name, _invited)
            .subscribe(function (newEvent) {
            _this.navCtrl.push(home_1.HomePage, {});
        });
    };
    NewgroupPage = __decorate([
        core_1.Component({
            templateUrl: 'build/pages/newgroup/newgroup.html',
            providers: [user_service_1.UserService, group_service_1.GroupService]
        }), 
        __metadata('design:paramtypes', [ionic_angular_1.NavController, ionic_angular_1.ToastController, user_service_1.UserService, group_service_1.GroupService])
    ], NewgroupPage);
    return NewgroupPage;
}());
exports.NewgroupPage = NewgroupPage;
