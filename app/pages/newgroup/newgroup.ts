import { Component } from '@angular/core';
import { NavController, ToastController } from 'ionic-angular';
import {UserService} from '../../providers/user-service/user-service';
import {GroupService} from '../../providers/group-service/group-service';
import {User} from '../../user';
import { HomePage } from '../home/home';


/*
  Generated class for the NewgroupPage page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
  */
  @Component({
  	templateUrl: 'build/pages/newgroup/newgroup.html',
  	providers: [UserService, GroupService]
  })
  export class NewgroupPage {

  	public users: User[];
  	public user: User;
  	public friends: User[];
  	public tempusers: User[];

  	constructor(private navCtrl: NavController, 
  		  		public toastCtrl: ToastController,
  		public userService: UserService,
  		public groupService: GroupService
  		 ) {
  		this.loadUsers();
  	}

  	loadUsers() {
  		this.userService.load()
  		.subscribe(userList => {
  			this.users = userList;
  			var elementPos = this.users.map(function(x) {return x.email; }).indexOf(window.localStorage.getItem('email'));
  			this.user = this.users[elementPos];
  			this.getFriends();

  		})
  	}

  	getFriends() { //retrieve all friends from user friend list
    this.friends = [];
    if(this.user.friends != null){
      for(var i =0; i < this.user.friends.length; i++){
           var elementPos = this.users.map(function(x) {return x.email; }).indexOf(this.user.friends[i]);
            this.friends.push(this.users[elementPos]);
          }
        }
      }
  	  	createGame(_name: string, _invited: string[]) {
  		if(_name == null){
  			let toast = this.toastCtrl.create({
  				message:'Please name your group!',
  				duration: 3000
  			});
  			toast.present();
  		}

  		this.tempusers = [];
  		_invited = _invited || [];
  		_invited.push(window.localStorage.getItem('email'));
  		
        //get all invited users and store the event to their database collection 
        for(var i =0; i < _invited.length; i++){
        	for(var j = 0; j < this.users.length; j++){

        		if(_invited[i] == this.users[j].email){
        			this.tempusers.push(this.users[j]);
        		}
        	}
        }

        for(var i =0; i < this.tempusers.length; i++){
        	this.tempusers[i].groups = this.tempusers[i].groups || [];
        	this.tempusers[i].groups.push(_name);
        	this.userService.update(this.tempusers[i])
        	.subscribe(res => {
        		if(_name == null){
        			let toast = this.toastCtrl.create({
        				message:'Group Created!',
        				duration: 3000
        			});
        			toast.present();
        		}
        	});


        }
//Save event to database
this.groupService.add(_name, _invited)
.subscribe(newEvent  => {
	this.navCtrl.push(HomePage, {});
});

}
  }
