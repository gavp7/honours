"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var ionic_angular_1 = require('ionic-angular');
var user_service_1 = require('../../providers/user-service/user-service');
/*
  Generated class for the NewsfeedPage page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
  */
var NewsfeedPage = (function () {
    function NewsfeedPage(userService, navCtrl) {
        this.userService = userService;
        this.navCtrl = navCtrl;
        this.loadUsers();
    }
    NewsfeedPage.prototype.doRefresh = function (refresher) {
        this.loadUsers();
        refresher.complete();
    };
    NewsfeedPage.prototype.loadUsers = function () {
        var _this = this;
        this.userService.load()
            .subscribe(function (userList) {
            _this.users = userList;
            var elementPos = _this.users.map(function (x) { return x.email; }).indexOf(window.localStorage.getItem('email'));
            _this.user = _this.users[elementPos];
            _this.GetPosts();
        });
    };
    NewsfeedPage.prototype.GetPosts = function () {
        this.news = this.user.news;
        var tempUser;
        for (var i = 0; i < this.user.friends.length; i++) {
            var tempUser;
            console.log(this.user.friends[i]);
            var elementPos = this.users.map(function (x) { return x.email; }).indexOf(this.user.friends[i]);
            tempUser = this.users[elementPos];
            if (tempUser.news != null) {
                for (var j = 0; j < tempUser.news.length; j++) {
                    if (this.news == null) {
                        this.news = [tempUser.news[j]];
                    }
                    else {
                        this.news.push(tempUser.news[j]);
                    }
                }
            }
            if (this.news != null) {
                this.news == this.news.reverse();
            }
        }
    };
    NewsfeedPage.prototype.PostReply = function (_message, _author, _date) {
        for (var i = 0; i < this.news.length; i++) {
            if (this.news[i].author == _author && this.news[i].date == _date) {
                if (this.news[i].comments == null) {
                    this.news[i].comments = [{ author: this.user.name, message: _message, date: new Date() }];
                }
                else {
                    this.news[i].comments.push({ author: this.user.name, message: _message, date: new Date() });
                }
                if (this.news[i].comments != null) {
                    this.news[i].comments == this.news[i].comments.reverse();
                }
                var tempUser;
                var elementPos = this.users.map(function (x) { return x.name; }).indexOf(_author);
                tempUser = this.users[elementPos];
                break;
            }
        }
        this.userService.update(tempUser)
            .subscribe(function (res) {
        });
    };
    NewsfeedPage.prototype.Post = function (message) {
        if (this.user.news == null) {
            this.user.news = [{ author: this.user.name, post: message, date: new Date(), comments: null }];
        }
        else {
            this.user.news.push({ author: this.user.name, post: message, date: new Date(), comments: null });
        }
        console.log(this.user.news);
        this.userService.update(this.user)
            .subscribe(function (res) {
        });
    };
    NewsfeedPage = __decorate([
        core_1.Component({
            templateUrl: 'build/pages/newsfeed/newsfeed.html',
            providers: [user_service_1.UserService]
        }), 
        __metadata('design:paramtypes', [user_service_1.UserService, ionic_angular_1.NavController])
    ], NewsfeedPage);
    return NewsfeedPage;
}());
exports.NewsfeedPage = NewsfeedPage;
