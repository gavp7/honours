//newsfeed.ts
//TODO: Like feature on posts?

import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import {UserService} from '../../providers/user-service/user-service';
import {User} from '../../user';
import {Observable} from 'rxjs/Observable';

@Component({
  templateUrl: 'build/pages/newsfeed/newsfeed.html',
  providers: [UserService]

})
export class NewsfeedPage {
  public users: User[];
  public user: User;
  public toggle: boolean;
  public news: [{ 
    author:string;
    post:string;
    date: string;
    comments: [{ author:string;
      message: string;
      date: string;
    }]	
  }]

  constructor(public userService: UserService,
    private navCtrl: NavController) {
    this.loadUsers(); 
    this.toggle == false;

    Observable.interval(1000 * 60).subscribe(x => {
      this.loadUsers(); //auto update every minute
      this.GetPosts();
    });
  }

  //refresh when page is dragged down
  doRefresh(refresher) {
    this.loadUsers();
    this.GetPosts();
    refresher.complete();
  }

  loadUsers() {
    this.userService.load()
    .subscribe(userList => {
      this.users = userList;
      var elementPos = this.users.map(function(x) {return x.email; }).indexOf(window.localStorage.getItem('email'));
      this.user = this.users[elementPos];
      this.GetPosts();
    })
  }

  //functions to toggle the comments page
  Toggle(){
    return this.toggle;
  }

  Switch(){
    if (this.toggle == false)
    {
      this.toggle = true;
    } else {
      this.toggle = false;
    }
  }

  GetPosts() { //retrieve all of the posts from all of the current users friends 
    this.news = this.user.news;
    var tempUser:User;
    for(var i =0; i < this.user.friends.length; i++){
      var tempUser:User;
      var elementPos = this.users.map(function(x) {return x.email; }).indexOf(this.user.friends[i]);
      tempUser = this.users[elementPos];
      if(tempUser.news != null){
        for(var j =0; j < tempUser.news.length; j++){
          if(this.news == null) {
            this.news = [tempUser.news[j]];
          }
          else { 
            this.news.push(tempUser.news[j]);
          }
        }
      }
      //order posts by date
      this.news.sort(function(a,b) { 
        return new Date(a.date).getTime() - new Date(b.date).getTime() 
      });

      this.news.reverse();
    }
  }

 //post reply
  PostReply(_message: string, _author: string, _date: string) {
    for (var i = 0; i < this.news.length; i++) {
      if(this.news[i].author == _author && this.news[i].date == _date){
        if(this.news[i].comments == null){
          this.news[i].comments = [{author:this.user.name, message: _message, date: new Date().toISOString()}];
        } else {
          this.news[i].comments.push({author:this.user.name, message: _message, date: new Date().toISOString()});
        }
        if(this.news[i].comments != null){
          this.news[i].comments == this.news[i].comments.reverse();
        }
        var tempUser: User;
        var elementPos = this.users.map(function(x) {return x.name; }).indexOf(_author);
        tempUser = this.users[elementPos];
        break;
      }
    }

    this.userService.update(tempUser)
    .subscribe(res => {
    });
  }

  //post message
  Post(message:string){
    if(this.user.news == null){
      this.user.news = [{author: this.user.name, post: message, date: new Date().toISOString(), comments:null }];
    }
    else{
      this.user.news.push({author: this.user.name, post: message, date: new Date().toISOString(), comments:null });
    }
    this.userService.update(this.user)
    .subscribe(res => {
    });
  }
}

