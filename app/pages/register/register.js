"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
//register.ts
var core_1 = require('@angular/core');
var ionic_angular_1 = require('ionic-angular');
var tabs_1 = require('../tabs/tabs');
var user_service_1 = require('../../providers/user-service/user-service');
var RegisterPage = (function () {
    function RegisterPage(userService, nav, toastCtrl) {
        this.userService = userService;
        this.nav = nav;
        this.toastCtrl = toastCtrl;
        this.loadUsers();
    }
    RegisterPage.prototype.loadUsers = function () {
        var _this = this;
        this.userService.load()
            .subscribe(function (userList) {
            _this.users = userList;
        });
    };
    RegisterPage.prototype.addUser = function (_name, _email, _city, _password) {
        var _this = this;
        if (_name == null || _email == null || _password == null) {
            var toast = this.toastCtrl.create({
                message: 'Please enter all fields',
                duration: 3000
            });
            toast.present();
        }
        else {
            if (this.checkEmail(_email) == false) {
                this.DuplicateUserToast();
            }
            else {
                this.userService.add(_name, _email, _city, _password)
                    .subscribe(function (newUser) {
                    _this.users.push(newUser);
                    _this.UserCreatedToast();
                    _this.nav.push(tabs_1.TabsPage);
                    window.localStorage.setItem('name', _name);
                    window.localStorage.setItem('email', _email);
                    window.localStorage.setItem('password', _password);
                    window.localStorage.setItem('city', _city);
                });
            }
        }
    };
    RegisterPage.prototype.checkEmail = function (_email) {
        var elementPos = this.users.map(function (x) { return x.email; }).indexOf(_email);
        var objectFound = this.users[elementPos];
        if (objectFound == null) {
            return true;
        }
        else {
            return false;
        }
    };
    RegisterPage.prototype.toggleComplete = function (user) {
        user.isComplete = !user.isComplete;
        this.userService.update(user)
            .subscribe(function (updatedUser) {
            user = updatedUser;
        });
    };
    RegisterPage.prototype.UserCreatedToast = function () {
        var toast = this.toastCtrl.create({
            message: 'Account Created!',
            duration: 3000
        });
        toast.present();
    };
    RegisterPage.prototype.DuplicateUserToast = function () {
        var toast = this.toastCtrl.create({
            message: 'The email address is already registered',
            duration: 3000
        });
        toast.present();
    };
    RegisterPage = __decorate([
        core_1.Component({
            templateUrl: 'build/pages/register/register.html',
            providers: [user_service_1.UserService]
        }), 
        __metadata('design:paramtypes', [user_service_1.UserService, ionic_angular_1.NavController, ionic_angular_1.ToastController])
    ], RegisterPage);
    return RegisterPage;
}());
exports.RegisterPage = RegisterPage;
