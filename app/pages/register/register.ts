//register.ts
import { Component } from '@angular/core';
import { CookieXSRFStrategy } from '@angular/http';
import { NavController, ToastController} from 'ionic-angular';
import { TabsPage } from '../tabs/tabs';
import {UserService} from '../../providers/user-service/user-service';
import {User} from '../../user';


@Component({
  templateUrl: 'build/pages/register/register.html',
  providers: [UserService]
})

export class RegisterPage {
  public users: User[];

  constructor(public userService: UserService,
    public nav: NavController,
    public toastCtrl: ToastController) {
    this.loadUsers();
  }

  loadUsers() {
    this.userService.load()
    .subscribe(userList => {
      this.users = userList;
    })
  }

  addUser(_name: string, _email: string, _city: string, _password: string) {
    var EMAIL_REGEXP = /^[a-z0-9!#$%&'*+\/=?^_`{|}~.-]+@[a-z0-9]([a-z0-9-]*[a-z0-9])?(\.[a-z0-9]([a-z0-9-]*[a-z0-9])?)*$/i;

    if (_email != "" && (_email.length <= 5 || !EMAIL_REGEXP.test(_email))) {
      let toast = this.toastCtrl.create({
        message:'Please enter a valid email address',
        duration: 3000
      });
      toast.present();        
    }
    else if(_name == null || _email == null || _password == null){
      let toast = this.toastCtrl.create({
        message:'Please enter all fields',
        duration: 3000
      });
      toast.present();
    }
    else{
      if(this.checkEmail(_email) == false)
      {
        this.DuplicateUserToast();
      }
      else 
      {

        this.userService.add(_name, _email, _city, _password)
        .subscribe(newUser  => {
          this.users.push(newUser);
          this.UserCreatedToast();
          this.nav.push(TabsPage);
          window.localStorage.setItem('name', _name);
          window.localStorage.setItem('email', _email);
          window.localStorage.setItem('password', _password);
          window.localStorage.setItem('city', _city);

        });
      }
    }
  }

  checkEmail(_email: string) {
    var elementPos = this.users.map(function(x) {return x.email; }).indexOf(_email);
    var objectFound = this.users[elementPos];
    if(objectFound == null){
      return true;
    }
    else{
      return false;

    }
  }
  
  toggleComplete(user: User) {
    user.isComplete = !user.isComplete;
    this.userService.update(user)
    .subscribe(updatedUser => {
      user = updatedUser;
    });
  }

  UserCreatedToast() {
    let toast = this.toastCtrl.create({
      message:'Account Created!',
      duration: 3000
    });
    toast.present();
  }

  DuplicateUserToast() {
    let toast = this.toastCtrl.create({
      message:'The email address is already registered',
      duration: 3000
    });
    toast.present();
  }
}

