"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var ionic_angular_1 = require('ionic-angular');
var user_service_1 = require('../../providers/user-service/user-service');
/*
  Generated class for the SettingsPage page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
var SettingsPage = (function () {
    function SettingsPage(navCtrl, userService, navParams) {
        this.navCtrl = navCtrl;
        this.userService = userService;
        this.navParams = navParams;
        this.loadUsers();
    }
    SettingsPage.prototype.loadUsers = function () {
        var _this = this;
        this.userService.load()
            .subscribe(function (userList) {
            _this.users = userList;
            var elementPos = _this.users.map(function (x) { return x.email; }).indexOf(window.localStorage.getItem('email'));
            _this.user = _this.users[elementPos];
        });
    };
    SettingsPage.prototype.saveChanges = function (_name, _email, _city, _password) {
        if (_name != undefined) {
            this.user.name = _name;
            window.localStorage.setItem('name', _name);
        }
        if (_email != undefined) {
            this.user.email = _email;
            window.localStorage.setItem('email', _email);
        }
        if (_city != undefined) {
            this.user.city = _city;
            window.localStorage.setItem('city', _city);
        }
        if (_password != undefined) {
            this.user.password = _password;
            window.localStorage.setItem('password', _password);
        }
        //save changes
        this.userService.update(this.user)
            .subscribe(function (res) {
            console.log('user updated');
        });
    };
    SettingsPage = __decorate([
        core_1.Component({
            templateUrl: 'build/pages/settings/settings.html',
            providers: [user_service_1.UserService]
        }), 
        __metadata('design:paramtypes', [ionic_angular_1.NavController, user_service_1.UserService, ionic_angular_1.NavParams])
    ], SettingsPage);
    return SettingsPage;
}());
exports.SettingsPage = SettingsPage;
