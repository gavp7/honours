//settings.ts
import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import {User} from '../../user';
import {UserService} from '../../providers/user-service/user-service';

@Component({
  templateUrl: 'build/pages/settings/settings.html',
  providers: [UserService]

})
export class SettingsPage {

  public users: User[];
  public user: User;

  constructor(private navCtrl: NavController,
    public userService: UserService,
    public navParams: NavParams) {
    this.loadUsers();
  }

  loadUsers() {
    this.userService.load()
    .subscribe(userList => {
      this.users = userList; 
      var elementPos = this.users.map(function(x) {return x.email; }).indexOf(window.localStorage.getItem('email'));
      this.user = this.users[elementPos];

    })
  }
  saveChanges(_name:string, _email: string, _city: string, _password: string){
    if(_name != undefined){
      this.user.name = _name;
      window.localStorage.setItem('name', _name);
    }
    if(_email != undefined) {
      this.user.email = _email;
      window.localStorage.setItem('email', _email);
    }
    if(_city != undefined) {
      this.user.city = _city;
      window.localStorage.setItem('city', _city);

    }
    if(_password != undefined) {
      this.user.password = _password;
      window.localStorage.setItem('password', _password);
    }

    //save changes
    this.userService.update(this.user)
    .subscribe(res => {
    });
  }
}