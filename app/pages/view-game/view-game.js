"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var ionic_angular_1 = require('ionic-angular');
var user_service_1 = require('../../providers/user-service/user-service');
var event_service_1 = require('../../providers/event-service/event-service');
/*
  Generated class for the ViewGamePage page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
  */
var ViewGamePage = (function () {
    function ViewGamePage(toastCtrl, userService, eventService, navCtrl, navParams) {
        this.toastCtrl = toastCtrl;
        this.userService = userService;
        this.eventService = eventService;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.loadUsers();
        console.log(this.event._id);
    }
    ViewGamePage.prototype.doRefresh = function (refresher) {
        this.loadUsers();
        refresher.complete();
    };
    ViewGamePage.prototype.loadUsers = function () {
        var _this = this;
        this.userService.load()
            .subscribe(function (userList) {
            _this.users = userList;
            var elementPos = _this.users.map(function (x) { return x.email; }).indexOf(window.localStorage.getItem('email'));
            _this.user = _this.users[elementPos];
        });
    };
    ViewGamePage.prototype.Check = function (attendee) {
        if (attendee == 'Slot Available') {
            this.AddPlayer();
        }
    };
    ViewGamePage.prototype.PostReply = function (_message, _author, _date) {
        for (var i = 0; i < this.event.posts.length; i++) {
            if (this.event.posts[i].author == _author && this.event.posts[i].date == _date) {
                if (this.event.posts[i].reply == null) {
                    this.event.posts[i].reply = [{ author: this.user.name, post: _message, date: new Date() }];
                }
                else {
                    this.event.posts[i].reply.push({ author: this.user.name, post: _message, date: new Date() });
                }
                if (this.event.posts[i].reply != null) {
                    this.event.posts[i].reply == this.event.posts[i].reply.reverse();
                }
                break;
            }
        }
        this.eventService.update(this.event)
            .subscribe(function (res) {
            //   this.navCtrl.push(ViewGamePage, {});
        });
    };
    ViewGamePage.prototype.PostMessage = function (_message) {
        console.log(this.event.name);
        if (this.event.posts == null) {
            this.event.posts = [{ author: this.user.name, post: _message, date: new Date(), reply: null }];
            console.log('hi');
        }
        else {
            this.event.posts.push({ author: this.user.name, post: _message, date: new Date(), reply: null });
            console.log('ho');
        }
        console.log('yo');
        this.eventService.update(this.event)
            .subscribe(function (res) {
            //    this.navCtrl.push(ViewGamePage, {event: this.event});
        });
        if (this.event.posts != null) {
            this.event.posts == this.event.posts.reverse();
        }
    };
    ViewGamePage.prototype.AddPlayer = function () {
        var playing = false;
        for (var i = 0; i < this.event.players; i++) {
            if (this.event.attending[i] == this.user.name) {
                var toast = this.toastCtrl.create({
                    message: 'You are already playing this game!',
                    duration: 3000
                });
                toast.present();
                playing = true;
            }
        }
        if (playing == false) {
            for (var i = 0; i < this.event.players; i++) {
                if (this.event.attending[i] == 'Slot Available') {
                    this.event.attending[i] = this.user.name;
                    break;
                }
            }
            this.eventService.update(this.event)
                .subscribe(function (res) {
                //    this.navCtrl.push(ViewGamePage, {event: this.event});
            });
        }
    };
    ViewGamePage = __decorate([
        core_1.Component({
            templateUrl: 'build/pages/view-game/view-game.html',
            providers: [user_service_1.UserService, event_service_1.EventService]
        }), 
        __metadata('design:paramtypes', [ionic_angular_1.ToastController, user_service_1.UserService, event_service_1.EventService, ionic_angular_1.NavController, ionic_angular_1.NavParams])
    ], ViewGamePage);
    return ViewGamePage;
}());
exports.ViewGamePage = ViewGamePage;
