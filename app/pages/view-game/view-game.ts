//view-game.ts
import { Component } from '@angular/core';
import { NavController, NavParams, ToastController } from 'ionic-angular';
import {UserService} from '../../providers/user-service/user-service';
import {User} from '../../user';
import {EventService } from '../../providers/event-service/event-service';
import { Event} from '../../Event';
import {Observable} from 'rxjs/Observable';

  @Component({
  	templateUrl: 'build/pages/view-game/view-game.html',
  	providers: [UserService, EventService]
  })
  export class ViewGamePage {
  	public event: Event;
  	public users: User[];
  	public user: User;
    public toggle: boolean;

    constructor(public toastCtrl: ToastController,
      public userService: UserService,
      public eventService: EventService,
      private navCtrl: NavController,
      private navParams: NavParams) {
      this.loadUsers(); 
      this.event = navParams.get('event');
      
      Observable.interval(1000 * 60).subscribe(x => {
        this.loadUsers();
        this.getEvent();
      });  		
    }
    doRefresh(refresher) {
      this.loadUsers();
      this.getEvent();
      refresher.complete();
    }

    loadUsers() {
      this.userService.load()
      .subscribe(userList => {
        this.users = userList;
        var elementPos = this.users.map(function(x) {return x.email; }).indexOf(window.localStorage.getItem('email'));
        this.user = this.users[elementPos];
      })
    }

    getEvent(){
      var events: Event[];
      this.eventService.load()
      .subscribe(eventList => {
        events = eventList;
        var elementPos = events.map(function(x) {return x._id; }).indexOf(this.event._id);
        this.event = events[elementPos];
      })
    }
    Check(attendee: string){
      if(attendee == 'Slot Available') {
        this.AddPlayer();
      }
    }

    Toggle(){
      return this.toggle;
    }

    Switch(){
      if (this.toggle == false)
        
      {
        this.toggle = true;
      } else {
        this.toggle = false;
      }
    }
    PostReply(_message: string, _author: string, _date: string) {
      for (var i = 0; i < this.event.posts.length; i++) {
        if(this.event.posts[i].author == _author && this.event.posts[i].date == _date){
          if(this.event.posts[i].reply == null){
            this.event.posts[i].reply = [{author:this.user.name, post: _message, date: new Date().toISOString()}];
          } else {
            this.event.posts[i].reply.push({author:this.user.name, post: _message, date: new Date().toISOString()});
          }
          if(this.event.posts[i].reply != null){
            this.event.posts[i].reply == this.event.posts[i].reply.reverse();
          }
          break;
        }
      }

      this.eventService.update(this.event)
      .subscribe(res => {
        //   this.navCtrl.push(ViewGamePage, {});

      });
    }

    PostMessage(_message: string){
      console.log(this.event.name);
      if(this.event.posts == null){
        this.event.posts = [{author: this.user.name, post: _message, date: new Date().toISOString(), reply:null}];
        console.log('hi');
      }
      else{
        this.event.posts.push({author: this.user.name, post: _message, date: new Date().toISOString(), reply:null});
        console.log('ho');
      }
      console.log('yo');
      this.eventService.update(this.event)
      .subscribe(res => {
        //    this.navCtrl.push(ViewGamePage, {event: this.event});
      });
      if(this.event.posts != null){
        this.event.posts == this.event.posts.reverse();
      }
    }

leaveGame() {
  var bool :boolean = false;
  for(var i = 0; i < this.event.attending.length; i++){
    if(this.event.attending[i] == this.user.name)
    {
      this.event.attending.splice(i, 1);
      this.event.attending.push('Slot Available');
      bool = true;
      break;
    }
  }
  if(bool == false){
    let toast = this.toastCtrl.create({
            message:'You are not yet playing this game!',
            duration: 3000
          });
          toast.present();
  }
   this.eventService.update(this.event)
        .subscribe(res => {
        });
}

    AddPlayer(){
      var playing: boolean = false;
      for(var i =0; i < this.event.players; i++){
        if(this.event.attending[i] == this.user.name){
          let toast = this.toastCtrl.create({
            message:'You are already playing this game!',
            duration: 3000
          });
          toast.present();
          playing = true;
        }
      }

      if(playing == false){
        for(var i =0; i < this.event.players; i++){
          if(this.event.attending[i] == 'Slot Available') {
            this.event.attending[i] = this.user.name;
            break;
          }
        }

        this.eventService.update(this.event)
        .subscribe(res => {
        });
      }
    }
  }
