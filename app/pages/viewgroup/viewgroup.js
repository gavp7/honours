"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var ionic_angular_1 = require('ionic-angular');
var view_game_1 = require('../view-game/view-game');
var new_game_1 = require('../new-game/new-game');
var group_service_1 = require('../../providers/group-service/group-service');
var user_service_1 = require('../../providers/user-service/user-service');
var event_service_1 = require('../../providers/event-service/event-service');
/*
  Generated class for the ViewgroupPage page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
var ViewgroupPage = (function () {
    function ViewgroupPage(navCtrl, navParams, groupService, userService, eventService) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.groupService = groupService;
        this.userService = userService;
        this.eventService = eventService;
        this.group = navParams.get('group');
        this.loadUsers();
        this.loadEvents();
    }
    ViewgroupPage.prototype.doRefresh = function (refresher) {
        this.loadUsers();
        this.loadEvents();
        refresher.complete();
    };
    ViewgroupPage.prototype.loadEvents = function () {
        var _this = this;
        this.eventService.load()
            .subscribe(function (eventList) {
            _this.events = eventList;
            _this.GetGroupEvents();
        });
    };
    ViewgroupPage.prototype.GetGroupEvents = function () {
        this.groupevents = this.groupevents || [];
        for (var i = 0; i < this.group.events.length; i++) {
            for (var j = 0; j < this.events.length; j++) {
                if (this.group.events[i] == this.events[j].name) {
                    this.groupevents.push(this.events[j]);
                }
            }
        }
    };
    ViewgroupPage.prototype.NewGame = function () {
        this.navCtrl.push(new_game_1.NewGamePage, { group: this.group });
    };
    ViewgroupPage.prototype.ViewGame = function (_event) {
        this.navCtrl.push(view_game_1.ViewGamePage, { event: _event });
    };
    ViewgroupPage.prototype.loadUsers = function () {
        var _this = this;
        this.userService.load()
            .subscribe(function (userList) {
            _this.users = userList;
            var elementPos = _this.users.map(function (x) { return x.email; }).indexOf(window.localStorage.getItem('email'));
            _this.user = _this.users[elementPos];
        });
    };
    ViewgroupPage.prototype.PostReply = function (_message, _author, _date) {
        for (var i = 0; i < this.group.posts.length; i++) {
            if (this.group.posts[i].author == _author && this.group.posts[i].date == _date) {
                if (this.group.posts[i].comments == null) {
                    this.group.posts[i].comments = [{ author: this.user.name, message: _message, date: new Date().toISOString() }];
                }
                else {
                    this.group.posts[i].comments.push({ author: this.user.name, message: _message, date: new Date().toISOString() });
                }
                if (this.group.posts[i].comments != null) {
                    this.group.posts[i].comments == this.group.posts[i].comments.reverse();
                }
                break;
            }
        }
        this.groupService.update(this.group)
            .subscribe(function (res) {
            //  this.navCtrl.push(ViewgroupPage, {group: this.group});
        });
    };
    ViewgroupPage.prototype.PostMessage = function (_message) {
        var _this = this;
        if (this.group.posts == null) {
            this.group.posts = [{ author: this.user.name, post: _message, date: new Date().toISOString(), comments: null }];
        }
        else {
            this.group.posts.push({ author: this.user.name, post: _message, date: new Date().toISOString(), comments: null });
        }
        this.groupService.update(this.group)
            .subscribe(function (res) {
            _this.navCtrl.push(ViewgroupPage, { group: _this.group });
        });
        if (this.group.posts != null) {
        }
    };
    ViewgroupPage = __decorate([
        core_1.Component({
            templateUrl: 'build/pages/viewgroup/viewgroup.html',
            providers: [group_service_1.GroupService, user_service_1.UserService, event_service_1.EventService]
        }), 
        __metadata('design:paramtypes', [ionic_angular_1.NavController, ionic_angular_1.NavParams, group_service_1.GroupService, user_service_1.UserService, event_service_1.EventService])
    ], ViewgroupPage);
    return ViewgroupPage;
}());
exports.ViewgroupPage = ViewgroupPage;
