//viewgroup.ts
import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import {Group} from '../../group';
import { ViewGamePage } from '../view-game/view-game';
import { NewGamePage } from '../new-game/new-game';
import {GroupService} from '../../providers/group-service/group-service';
import {UserService } from '../../providers/user-service/user-service';
import {EventService} from '../../providers/event-service/event-service';
import {Event} from '../../Event';
import {User} from '../../user';
import {Observable} from 'rxjs/Observable';

  @Component({
  	templateUrl: 'build/pages/viewgroup/viewgroup.html',
  	providers: [GroupService, UserService, EventService]
  })

  export class ViewgroupPage {
  	public group: Group;
  	public events: Event[];
  	public user: User;
  	public users: User[];
  	public groupevents: Event[];
  	public toggle: boolean;

  	constructor(private navCtrl: NavController,
  		private navParams: NavParams,
  		public groupService: GroupService,
  		public userService: UserService,
  		public eventService: EventService) {
  		this.group = navParams.get('group');
  		this.loadUsers();
  		this.loadEvents();
  		Observable.interval(1000 * 60).subscribe(x => {
  			this.loadUsers();
  			this.loadEvents();
  		});  		
  	}

  	doRefresh(refresher) {
  		this.loadUsers();
  		this.loadEvents();
  		refresher.complete();
  	}

  	loadEvents() {
  		this.eventService.load()
  		.subscribe(eventList => {
  			this.events = eventList;
  			this.GetGroupEvents();
  		})
  	}

  	GetGroupEvents() {
  		if(this.group.events != undefined){
  			for(var i =0; i < this.group.events.length; i++) {
          var elementPos = this.group.events.map(function(x) {return x; }).indexOf(this.events[i].name);
  				if(this.groupevents == null){
  					this.groupevents = [this.events[i]];
  				} else {
  					this.groupevents.push(this.events[i]);
  				}
  			}
  		}
  	}
  
  	

  	Toggle(){
  		return this.toggle;
  	}

  	Switch(){
  		if (this.toggle == false)
  		{
  			this.toggle = true;
  		} else {
  			this.toggle = false;
  		}
  	}

  	NewGame() {
  		this.navCtrl.push(NewGamePage, {group : this.group});
  	}

  	ViewGame(_event: Event) {
  		this.navCtrl.push(ViewGamePage, { event: _event});
  	}

  	loadUsers() {
  		this.userService.load()
  		.subscribe(userList => {
  			this.users = userList;
  			var elementPos = this.users.map(function(x) {return x.email; }).indexOf(window.localStorage.getItem('email'));
  			this.user = this.users[elementPos];
  		})
  	}

  	PostReply(_message: string, _author: string, _date: string) {
  		for (var i = 0; i < this.group.posts.length; i++) {
  			if(this.group.posts[i].author == _author && this.group.posts[i].date == _date){
  				if(this.group.posts[i].comments == null){
  					this.group.posts[i].comments = [{author:this.user.name, message: _message, date: new Date().toISOString()}];
  				} else {
  					this.group.posts[i].comments.push({author:this.user.name, message: _message, date: new Date().toISOString()});
  				}
  				if(this.group.posts[i].comments != null){
  					this.group.posts[i].comments == this.group.posts[i].comments.reverse();
  				}
  				break;
  			}
  		}
  		this.groupService.update(this.group)
  		.subscribe(res => {
  		});
  	}

  	PostMessage(_message: string){
  		if(this.group.posts == null){
  			this.group.posts = [{author: this.user.name, post: _message, date: new Date().toISOString(), comments:null}];
  		}
  		else{
  			this.group.posts.push({author: this.user.name, post: _message, date: new Date().toISOString(), comments:null});
  		}
  		this.groupService.update(this.group)
  		.subscribe(res => {
  			this.navCtrl.push(ViewgroupPage, {group: this.group});
  		});
  	}
  }
