"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var http_1 = require('@angular/http');
var Observable_1 = require('rxjs/Observable');
require('rxjs/Rx');
require('rxjs/add/operator/map');
var EventService = (function () {
    function EventService(http) {
        this.http = http;
        //eventUrl = "http://localhost:8080/api/events"
        this.eventUrl = "http://35.160.166.212:8080/api/events";
    }
    // Get all Events
    EventService.prototype.load = function () {
        return this.http.get(this.eventUrl)
            .map(function (res) { return res.json(); })
            .catch(this.handleError);
    };
    // Add an event-edit
    EventService.prototype.add = function (_name, _sport, _players, _date, _starttime, _endtime, _location, _invited, _attending) {
        var body = JSON.stringify({ name: _name, sport: _sport, players: _players, date: _date, starttime: _starttime, endtime: _endtime, location: _location, invited: _invited, attending: _attending });
        var headers = new http_1.Headers({ 'Content-Type': 'application/json' });
        return this.http.post(this.eventUrl, body, { headers: headers })
            .map(function (res) { return res.json(); })
            .catch(this.handleError);
    };
    // Update an event
    EventService.prototype.update = function (event) {
        var url = this.eventUrl + "/" + event._id;
        var body = JSON.stringify(event);
        var headers = new http_1.Headers({ 'Content-Type': 'application/json' });
        console.log(url, body);
        return this.http.put(url, body, { headers: headers })
            .map(function () { return event; }) //See mdn.io/arrowfunctions
            .catch(this.handleError);
    };
    // Delete an event
    EventService.prototype.delete = function (event) {
        var url = this.eventUrl + "/" + event._id;
        var headers = new http_1.Headers({ 'Content-Type': 'application/json' });
        return this.http.delete(url, headers)
            .catch(this.handleError);
    };
    EventService.prototype.handleError = function (error) {
        console.error(error);
        return Observable_1.Observable.throw(error.json().error || 'Server error');
    };
    EventService = __decorate([
        core_1.Injectable(), 
        __metadata('design:paramtypes', [http_1.Http])
    ], EventService);
    return EventService;
}());
exports.EventService = EventService;
