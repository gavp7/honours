//event-service.ts
import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { Event } from '../../event'
import {Observable} from 'rxjs/Observable';
import 'rxjs/Rx';
import 'rxjs/add/operator/map';

@Injectable()
export class EventService {
  //eventUrl = "http://localhost:8080/api/events"
  eventUrl = "http://35.160.166.212:8080/api/events"
  constructor(private http: Http) {}

// Get all Events
  load(): Observable<Event[]> {
    return this.http.get(this.eventUrl)
               .map(res => res.json())
               .catch(this.handleError);
  }
  

  // Add an event-edit
  add(_name: string,_sport: string, _players: number, _date: string, _starttime: string, _endtime: string, _location: string, _invited: string[], _attending: string[]): Observable<Event> {
    let body = JSON.stringify({name: _name, sport: _sport, players: _players, date: _date, starttime: _starttime, endtime: _endtime, location: _location, invited: _invited, attending: _attending});
    let headers = new Headers({'Content-Type': 'application/json'});

    return this.http.post(this.eventUrl, body, {headers: headers})
                    .map(res => res.json())
                    .catch(this.handleError);
  }
  // Update an event
  update(event: Event) {
    let url = `${this.eventUrl}/${event._id}`;
    let body = JSON.stringify(event);
    let headers = new Headers({'Content-Type': 'application/json'});
    console.log(url, body);
    return this.http.put(url, body, {headers: headers})
                    .map(() => event) //See mdn.io/arrowfunctions
                    .catch(this.handleError);

  }

  // Delete an event
  delete(event: Event) {
    let url = `${this.eventUrl}/${event._id}`;
    let headers = new Headers({'Content-Type': 'application/json'});

    return this.http.delete(url, headers)
               .catch(this.handleError);
  }

  handleError(error) {
      console.error(error);
      return Observable.throw(error.json().error || 'Server error');
  }
}

