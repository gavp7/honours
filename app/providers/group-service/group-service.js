"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var http_1 = require('@angular/http');
var Observable_1 = require('rxjs/Observable');
require('rxjs/Rx');
require('rxjs/add/operator/map');
var GroupService = (function () {
    function GroupService(http) {
        this.http = http;
        //groupUrl = "http://localhost:8080/api/groups"
        this.groupUrl = "http://35.160.166.212:8080/api/groups";
    }
    // Get all Groups
    GroupService.prototype.load = function () {
        return this.http.get(this.groupUrl)
            .map(function (res) { return res.json(); })
            .catch(this.handleError);
    };
    // Add a user-edit
    GroupService.prototype.add = function (_name, _members) {
        var body = JSON.stringify({ name: _name, members: _members });
        var headers = new http_1.Headers({ 'Content-Type': 'application/json' });
        return this.http.post(this.groupUrl, body, { headers: headers })
            .map(function (res) { return res.json(); })
            .catch(this.handleError);
    };
    // Update a group
    GroupService.prototype.update = function (group) {
        var url = this.groupUrl + "/" + group._id;
        var body = JSON.stringify(group);
        var headers = new http_1.Headers({ 'Content-Type': 'application/json' });
        console.log(url, body);
        return this.http.put(url, body, { headers: headers })
            .map(function () { return group; }) //See mdn.io/arrowfunctions
            .catch(this.handleError);
    };
    // Delete a group
    GroupService.prototype.delete = function (group) {
        var url = this.groupUrl + "/" + group._id;
        var headers = new http_1.Headers({ 'Content-Type': 'application/json' });
        return this.http.delete(url, headers)
            .catch(this.handleError);
    };
    GroupService.prototype.handleError = function (error) {
        console.error(error);
        return Observable_1.Observable.throw(error.json().error || 'Server error');
    };
    GroupService = __decorate([
        core_1.Injectable(), 
        __metadata('design:paramtypes', [http_1.Http])
    ], GroupService);
    return GroupService;
}());
exports.GroupService = GroupService;
