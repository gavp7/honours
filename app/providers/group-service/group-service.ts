//group-service.ts
import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import {Group } from '../../group';
import {Observable} from 'rxjs/Observable';
import 'rxjs/Rx';
import 'rxjs/add/operator/map';

@Injectable()
export class GroupService {
	 //groupUrl = "http://localhost:8080/api/groups"
  groupUrl = "http://35.160.166.212:8080/api/groups"
  constructor(private http: Http) {}

// Get all Groups
  load(): Observable<Group[]> {
    return this.http.get(this.groupUrl)
               .map(res => res.json())
               .catch(this.handleError);
  }
  
  // Add a user-edit
  add(_name: string, _members: string[]): Observable<Group> {
    let body = JSON.stringify({name: _name, members: _members });
    let headers = new Headers({'Content-Type': 'application/json'});

    return this.http.post(this.groupUrl, body, {headers: headers})
                    .map(res => res.json())
                    .catch(this.handleError);
  }

    // Update a group
  update(group: Group) {
    console.log(group._id);
    console.log('update function called');
    let url = `${this.groupUrl}/${group._id}`;
    let body = JSON.stringify(group);
    let headers = new Headers({'Content-Type': 'application/json'});
    console.log(url, body);
    return this.http.put(url, body, {headers: headers})
                    .map(() => group) //See mdn.io/arrowfunctions
                    .catch(this.handleError);
  }

   // Delete a group
  delete(group: Group) {
    let url = `${this.groupUrl}/${group._id}`;
    let headers = new Headers({'Content-Type': 'application/json'});

    return this.http.delete(url, headers)
               .catch(this.handleError);
  }

    handleError(error) {
      console.error(error);
      return Observable.throw(error.json().error || 'Server error');
  }
}

