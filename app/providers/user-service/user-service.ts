//user-service.ts
import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import {Observable} from 'rxjs/Observable';
import {User} from '../../user';
import 'rxjs/Rx';
import 'rxjs/add/operator/map';

@Injectable()
export class UserService {
  //usersUrl = "http://localhost:8080/api/users"
  usersUrl = "http://35.160.166.212:8080/api/users";
  constructor(public http: Http) {}

  // Get all users
  load(): Observable<User[]> {
    return this.http.get(this.usersUrl)
               .map(res => res.json())
               .catch(this.handleError);
  }
  
  // Add a user-edit
  add(_name: string, _email: string, _city: string, _password: string): Observable<User> {
    let body = JSON.stringify({name: _name, email: _email, city: _city, password: _password});
    let headers = new Headers({'Content-Type': 'application/json'});

    return this.http.post(this.usersUrl, body, {headers: headers})
                    .map(res => res.json())
                    .catch(this.handleError);
  }
  
  // Update a user
  update(user: User) {
    let url = `${this.usersUrl}/${user._id}`;
    let body = JSON.stringify(user);
    let headers = new Headers({'Content-Type': 'application/json'});
    console.log(url, body);
    return this.http.put(url, body, {headers: headers})
                    .map(() => user) //See mdn.io/arrowfunctions
                    .catch(this.handleError);

  }

  // Delete a user
  delete(user: User) {
    let url = `${this.usersUrl}/${user._id}`;
    let headers = new Headers({'Content-Type': 'application/json'});

    return this.http.delete(url, headers)
               .catch(this.handleError);
  }

  handleError(error) {
      console.error(error);
      return Observable.throw(error.json().error || 'Server error');
  }
}