//user.ts
export class User { 
	_id: number;
	name: string;
	city: string;
	email: string;
	password: string;
	friends: string[];
	friendrequests: string[];
	events: string[];
	groups: string [];
	isComplete: boolean;
	news: [{ 
		author:string;
		post:string;
		date: string;
		comments: [{ author:string;
			message: string;
			date: string;
		}]	
	}]

}

