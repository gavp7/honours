var express = require('express');
var bodyParser = require('body-parser');
var cors = require('cors');
var app = express();

var mongodb = require('mongodb'),
    mongoClient = mongodb.MongoClient,
    ObjectID = mongodb.ObjectID, // Used in API endpoints
    db; // We'll initialize connection below

app.use(bodyParser.json());
app.set('port', process.env.PORT || 8080);
app.use(cors()); // CORS (Cross-Origin Resource Sharing) headers to support Cross-site HTTP requests
app.use(express.static("www")); // Our Ionic app build is in the www folder (kept up-to-date by the Ionic CLI using 'ionic serve')

var MONGODB_URI = process.env.MONGODB_URI || 'mongodb://Admin:Spacejam1@ds139267.mlab.com:39267/honours';
//var MONGODB_URI = process.env.MONGODB_URI || 'mongodb://localhost:27017';

// Initialize database connection and then start the server.
mongoClient.connect(MONGODB_URI, function (err, database) {
  if (err) {
    process.exit(1);
  }

  db = database; 

  console.log("Database connection ready");

  // Initialize the app.
  app.listen(app.get('port'), function () {
    console.log("listening on port", app.get('port'));
  });
});

// API Routes Will Go Below
/*
 *  Endpoint --> "/api/users"
 */

// GET: retrieve all users
app.get("/api/users", function(req, res) {

  db.collection("users").find({}).toArray(function(err, docs) {
    if (err) {
      handleError(res, err.message, "Failed to get users");
    } else {
      res.status(200).json(docs);
    }
  });
});

// POST: create a new user
app.post("/api/users", function(req, res) {
  console.log('hi');
  var newUser = {
    name: req.body.name,
    email: req.body.email,
    city: req.body.city,
    password: req.body.password,
    friends: [],
friendrequests: [],
    isComplete: false
  }
  db.collection("users").insert(newUser, function(err, doc) {
    if (err) {
      handleError(res, err.message, "Failed to add user");
    } else {
      res.status(201).json(doc.ops[0]);
    }
  });
});



// GET: retrieve a user by id -- Note, not used on front-end
app.get("/api/users/:id", function(req, res) {
  console.log('hi');
  db.collection("users").findOne({ _id: new ObjectID(req.params.id) }, function(err, doc) {
    if (err) {
      handleError(res, err.message, "Failed to get user by _id");
    } else {
      res.status(200).json(doc);
    }
  });
});

// PUT: update a uesr by id
app.put("/api/users/:id", function(req, res) {
  console.log('hi');
  var updateUser = req.body;
  delete updateUser._id;

  db.collection("users").updateOne({_id: new ObjectID(req.params.id)}, updateUser, function(err, doc) {
    if (err) {
      handleError(res, err.message, "Failed to update user");
    } else {
      res.status(204).end();
    }
  });
});
// DELETE: delete a user by id
app.delete("/api/users/:id", function(req, res) {
  db.collection("users").deleteOne({_id: new ObjectID(req.params.id)}, function(err, result) {
    if (err) {
      handleError(res, err.message, "Failed to delete user");
    } else {
      res.status(204).end();
    }
  });
});

/*
 *  Endpoint --> "/api/events"
 */
// GET: retrieve all events
app.get("/api/events", function(req, res) {

  db.collection("events").find({}).toArray(function(err, docs) {
    if (err) {
      handleError(res, err.message, "Failed to get events");
    } else {
      res.status(200).json(docs);
    }
  });
});

// POST: create a new event
app.post("/api/events", function(req, res) {
  console.log('adding event');
  var newEvent = {
    name: req.body.name,
    sport: req.body.sport,
    players: req.body.players,
    date: req.body.date,
    starttime: req.body.starttime,
    endtime: req.body.endtime,
    location: req.body.location,
    invited: req.body.invited,
    attending: req.body.attending,
    posts: null
  }

  db.collection("events").insert(newEvent, function(err, doc) {
    if (err) {
      handleError(res, err.message, "Failed to add event");
    } else {
      res.status(201).json(doc.ops[0]);
      console.log('Added');
    }
  });
});



// GET: retrieve an event by id -- Note, not used on front-end
app.get("/api/events/:id", function(req, res) {
  console.log('hi');
  db.collection("events").findOne({ _id: new ObjectID(req.params.id) }, function(err, doc) {
    if (err) {
      handleError(res, err.message, "Failed to get event by _id");
    } else {
      res.status(200).json(doc);
    }
  });
});

// PUT: update an event by id
app.put("/api/events/:id", function(req, res) {
  console.log('hi');
  var updateEvent = req.body;
  delete updateEvent._id;

  db.collection("events").updateOne({_id: new ObjectID(req.params.id)}, updateEvent, function(err, doc) {
    if (err) {
      handleError(res, err.message, "Failed to update event");
    } else {
      res.status(204).end();
    }
  });
});
// DELETE: delete an event by id
app.delete("/api/events/:id", function(req, res) {
  db.collection("events").deleteOne({_id: new ObjectID(req.params.id)}, function(err, result) {
    if (err) {
      handleError(res, err.message, "Failed to delete event");
    } else {
      res.status(204).end();
    }
  });
});

/*
 *  Endpoint --> "/api/groups"
 */
// GET: retrieve all groups
app.get("/api/groups", function(req, res) {

  db.collection("groups").find({}).toArray(function(err, docs) {
    if (err) {
      handleError(res, err.message, "Failed to get groups");
    } else {
      res.status(200).json(docs);
    }
  });
});

// POST: create a new group
app.post("/api/groups", function(req, res) {
  console.log('adding groups');
  var newGroup = {
    name: req.body.name,
    members: req.body.members,
    events: null,
        posts: null
  }

  db.collection("groups").insert(newEvent, function(err, doc) {
    if (err) {
      handleError(res, err.message, "Failed to add groups");
    } else {
      res.status(201).json(doc.ops[0]);
      console.log('Added');
     
    }
  });
});



// GET: retrieve a group by id -- Note, not used on front-end
app.get("/api/groups/:id", function(req, res) {
  db.collection("groups").findOne({ _id: new ObjectID(req.params.id) }, function(err, doc) {
    if (err) {
      handleError(res, err.message, "Failed to get groups by _id");
    } else {
      res.status(200).json(doc);
    }
  });
});

// PUT: update a group by id
app.put("/api/groups/:id", function(req, res) {
  var updateGroup = req.body;
  delete updateGroup._id;

  db.collection("groups").updateOne({_id: new ObjectID(req.params.id)}, updateGroup, function(err, doc) {
    if (err) {
      handleError(res, err.message, "Failed to update group");
    } else {
    }
  });
});
// DELETE: delete a group by id
app.delete("/api/groups/:id", function(req, res) {
  db.collection("groups").deleteOne({_id: new ObjectID(req.params.id)}, function(err, result) {
    if (err) {
      handleError(res, err.message, "Failed to delete group");
    } else {
      res.status(204).end();
    }
  });

      res.status(204).end();});
// Error handler for the api
function handleError(res, reason, message, code) {
  console.log("API Error: " + reason);
  res.status(code || 500).json({"Error": message});
}

